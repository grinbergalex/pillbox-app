//
//  HistoryMedicineCell.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 10/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class HistoryMedicineCell: UITableViewCell {
    
    @IBOutlet weak var dateLabelText: UILabel!
    @IBOutlet weak var medicationDosageLabel: UILabel!
    @IBOutlet weak var medicationReminderLabel: UILabel!
    @IBOutlet weak var medicationButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
