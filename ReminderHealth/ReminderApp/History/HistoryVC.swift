//
//  HistoryVC.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 03/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase

class HistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var medicineNameLabel: UILabel!
    
    var infodetail = UserMeds()
    
    var text = ""
    
    var refreshControl = UIRefreshControl()
    
    var itemUID = ""
    
    var historyModel = [History]()
    let uid = Auth.auth().currentUser?.uid

    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        fetchHistory()
        
    

    }
    
    @objc func refresh(sender:AnyObject) {
        DispatchQueue.main.async(execute: {
            self.fetchHistory()
            self.refreshControl.endRefreshing()
            
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        self.itemUID = infodetail.medIndex
        self.medicineNameLabel.text = infodetail.medName
        
        let values = ["medicationName": infodetail.medName, "medicationDosage": infodetail.medDosage, "medicationIndex":infodetail.medIndex, "medicationReminder": infodetail.medIndex]
        
        let ref = Database.database().reference().child("history")
        ref.child(uid!).child(infodetail.medIndex).updateChildValues(values, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print("error")
            } else {
                print("worked")
            }
        })
     /*infodetail.medIndex*/
        
     //   print(Date().dayOfWeek()!) // Wednesday
      //  dateLabel.text = Date().dayOfWeek()!
        
        print(itemUID)
        
        
        
        
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    
    func fetchHistory() {
        
        let medicationDB = Database.database().reference().child("history").child(uid!)
        medicationDB.child(itemUID).observe(DataEventType.childAdded, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                
                let savedMedications = History()
                
                savedMedications.medicineName = (dictionary["medicationName"] as! String?)!
                savedMedications.medicineDosage = (dictionary["medicationDosage"] as! String?)!
                savedMedications.medicineIndex = (dictionary["medicationIndex"] as! String?)!
                //savedMedications. = (dictionary["medicationReminder"] as! String?)!
                self.historyModel.append(savedMedications)
                
                DispatchQueue.main.async(execute: {
                    self.tableView?.reloadData()
                    
                })
            }
            print(snapshot)
            
            
        }, withCancel: nil)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! HistoryCell
        
        let historyIndex = historyModel[indexPath.row]
        
        cell.medicineDosageLabel.text = historyIndex.medicineDosage
        cell.dateLabel.text = historyIndex.medicineIndex

        
        return cell
    }

}
