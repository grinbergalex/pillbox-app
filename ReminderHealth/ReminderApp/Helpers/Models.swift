//
//  UserMedsModel.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 18/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit

class UserMeds: NSObject {
    var medName = ""
    var medDosage = ""
    var medFrequency = ""
    var medReminder = ""
    var medIndex = ""
}

class UserItemsInfo: NSObject {
    
    var name = ""
    var pictureURL = ""
    var email = ""
    var password = ""
    var profileImage = UIImageView()
    var age = ""
    var weight = ""
    
}

class MedicineHistory: NSObject {
    
    var medicineName = ""
    var dateLabel = ""
    var medicineDosage = ""
    var medicationReminder = ""
    var medicationIndex = ""
}
