//
//  Extensions.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 03/11/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//


import UIKit

extension UIColor {
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    static func mainBlue() -> UIColor {
        return UIColor.rgb(red: 17, green: 154, blue: 237)
    }
    
    convenience init(_ hex: UInt) {
            self.init(
                red: CGFloat((hex & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((hex & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(hex & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }
    
    typealias GradientType = (x: CGPoint, y: CGPoint)
    
    enum GradientPoint {
        case leftRight
        case rightLeft
        case topBottom
        case bottomTop
        case topLeftBottomRight
        case bottomRightTopLeft
        case topRightBottomLeft
        case bottomLeftTopRight
        
        func draw() -> GradientType {
            switch self {
            case .leftRight:
                return (x: CGPoint(x: 0, y: 0.5), y: CGPoint(x: 1, y: 0.5))
            case .rightLeft:
                return (x: CGPoint(x: 1, y: 0.5), y: CGPoint(x: 0, y: 0.5))
            case .topBottom:
                return (x: CGPoint(x: 0.5, y: 0), y: CGPoint(x: 0.5, y: 1))
            case .bottomTop:
                return (x: CGPoint(x: 0.5, y: 1), y: CGPoint(x: 0.5, y: 0))
            case .topLeftBottomRight:
                return (x: CGPoint(x: 0, y: 0), y: CGPoint(x: 1, y: 1))
            case .bottomRightTopLeft:
                return (x: CGPoint(x: 1, y: 1), y: CGPoint(x: 0, y: 0))
            case .topRightBottomLeft:
                return (x: CGPoint(x: 1, y: 0), y: CGPoint(x: 0, y: 1))
            case .bottomLeftTopRight:
                return (x: CGPoint(x: 0, y: 1), y: CGPoint(x: 1, y: 0))
            }
        }
    }
    
    class GradientLayer : CAGradientLayer {
        var gradient: GradientType? {
            didSet {
                startPoint = gradient?.x ?? CGPoint.zero
                endPoint = gradient?.y ?? CGPoint.zero
            }
        }
    }
    
}

    protocol GradientViewProvider {
        associatedtype GradientViewType
}   


extension UIView {
    func anchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?,  paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
}

extension Date {
    func timeAgoDisplay() -> String {
        let secondsAgo = Int(Date().timeIntervalSince(self))
        
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        let month = 4 * week
        
        let quotient: Int
        let unit: String
        if secondsAgo < minute {
            quotient = secondsAgo
            unit = "second"
        } else if secondsAgo < hour {
            quotient = secondsAgo / minute
            unit = "min"
        } else if secondsAgo < day {
            quotient = secondsAgo / hour
            unit = "hour"
        } else if secondsAgo < week {
            quotient = secondsAgo / day
            unit = "day"
        } else if secondsAgo < month {
            quotient = secondsAgo / week
            unit = "week"
        } else {
            quotient = secondsAgo / month
            unit = "month"
        }
        
        return "\(quotient) \(unit)\(quotient == 1 ? "" : "s") ago"
        
    }
}


