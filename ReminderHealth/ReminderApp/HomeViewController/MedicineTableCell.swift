//
//  MedicineTableCell.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 05/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

class MedicineTableCell: UITableViewCell {
    
    var infodetail = UserMeds()
    
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var dosage: UILabel!
    @IBOutlet weak var reminder: UILabel!
    @IBOutlet weak var takeButtonPressed: UIButton!
    @IBOutlet weak var bgView: GradientView!
    
    let uid = Auth.auth().currentUser?.uid
    
    var itemUid = ""

    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        if takeButtonPressed.isSelected {
            takeButtonPressed.setImage(UIImage(named: "checked"), for: .normal)
        }

        itemUid = infodetail.medIndex
        
    }
    
    
     func viewWillAppear(_ animated: Bool) {
        
        

        
      //  print(Date().dayOfWeek()!) // Wednesday
        
        
    }
    
    
    @IBAction func buttonSelected(_ sender: UIButton) {
        
        if takeButtonPressed.isSelected == false {
         
        takeButtonPressed.setImage(UIImage(named: "checked"), for: .normal)
            
            showNotification()
    
//            let values = ["MedicationName": name.text!, "MedicationDosage": dosage.text!, "medicationReminder": reminder.text!, "isTaken": true] as [String : Any]
//
//            let ref = Database.database().reference().child("history").child(uid!)
//            ref.child(infodetail.medIndex).updateChildValues(values, withCompletionBlock: {(error, ref) in
//
//                if error != nil {
//                    print("The medication was taken")
//                }
//
//            })
            
        } else {
            self.takeButtonPressed.setImage(UIImage(named: "5578_Icon_VC-7"), for: .normal)
        }
            

    }
    
    func showNotification() {
        
        var date = ""
        let center = UNUserNotificationCenter.current()
        
        let content = UNMutableNotificationContent()
        content.title = "Great!"
        content.body = "Medication named \(name.text) was take at \(date)"
        content.categoryIdentifier = "alarm"
        content.sound = UNNotificationSound.default
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        print(dateFormatter.string(from: Date())) // prints "Oct 28, 2017"
        
        date = dateFormatter.string(from: Date())
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
        let request = UNNotificationRequest(identifier: "alarm", content: content, trigger: trigger)
        center.add(request)
    }

}
