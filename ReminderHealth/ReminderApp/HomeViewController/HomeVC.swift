//
//  MainMedicationPage.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 18/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var userMedication = [UserMeds]()
    var userInformation = [UserItemsInfo]()
    
    var notificationIndex: Int!
    
    @IBOutlet weak var dateNow: UILabel!
    @IBOutlet weak var profileView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var backgroundNoData: UIView!
    
    
    @IBOutlet weak var backButton: UIButton!
    
    var refreshControl = UIRefreshControl()
    
    
    let medicationIndexKey = Database.database().reference().childByAutoId().key
    
    var medicationIndex: String?
    
    let uid = Auth.auth().currentUser?.uid
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = .clear
        
        self.notificationIndex = 0
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        hideKeyboardWhenTappedAround()
        
        if uid == nil {
            
            let vc = RegisterMenu()
            self.present(vc, animated: true, completion: nil)
            
        }
        fetchUserMedication()

//        fetchUserInfo()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        print(dateFormatter.string(from: Date())) // prints "Oct 28, 2017"
        
        self.dateNow.text = dateFormatter.string(from: Date())
        
    }
    
    @objc func refresh(sender:AnyObject) {
        DispatchQueue.main.async(execute: {
            
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
            
        })
    }
    
    func fetchUserInfo() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let childId = Database.database().reference().child("users").child(uid)
        childId.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let index = UserItemsInfo()
                index.pictureURL = ((dictionary["profileImageUrl"]) as? String)!
                index.name = ((dictionary["name"]) as? String)!
                
                self.userInformation.append(index)
                
                let url = URLRequest(url: URL(string: index.pictureURL)!)
                
                let task = URLSession.shared.dataTask(with: url) {
                    (data, response, error) in
                    
                    if error != nil {
                        print(error!)
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.profileView.image = UIImage(data: data!)
                    }
                }
                task.resume()
            }
            print(snapshot)
            
        }, withCancel: nil)
    }
    
    
    
    func fetchUserMedication() {
        
        let medicationDB = Database.database().reference().child("medications").child(uid!)
        medicationDB.observe(DataEventType.childAdded, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                
                let savedMedications = UserMeds()

                savedMedications.medName = (dictionary["medicationName"] as! String?)!
                savedMedications.medDosage = (dictionary["medicationDosage"] as! String?)!
                savedMedications.medIndex = (dictionary["medProvider"] as! String?)!
                savedMedications.medReminder = (dictionary["medicationReminder"] as! String?)!
                self.userMedication.append(savedMedications)
                
                self.medicationIndex = savedMedications.medIndex

                
                DispatchQueue.main.async(execute: {
                    self.tableView?.reloadData()
                    
                })
            }
            print(snapshot)
            print(self.medicationIndex)
            
            
        }, withCancel: nil)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userMedication.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MedicineTableCell", for: indexPath) as! MedicineTableCell
        
        let medication = userMedication[indexPath.row]
        cell.name.text = medication.medName
        cell.dosage.text = medication.medDosage
        cell.reminder.text = "Your reminder is setup to \(medication.medReminder)"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
   
    
   func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            //TODO: edit the row at indexPath here
        }
        editAction.backgroundColor = .blue
 
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
            //TODO: Delete the row at indexPath here
            let DB_TODELETE = Database.database().reference().child("medications").child(self.uid!)
            DB_TODELETE.child(self.medicationIndex!).removeValue()
            
            let DB_History = Database.database().reference().child("history").child(self.uid!)
            DB_History.child(self.medicationIndex!).removeValue()
            
            
            //////////////////////////////////////////////////////////////
            ///MARK: Need to understand if the index autoincrement is/////
            //necessary for the delete or add of new medications//////////
            
            let center = UNUserNotificationCenter.current()
            center.removePendingNotificationRequests(withIdentifiers: ["alarm_index\(self.notificationIndex = self.notificationIndex + 1)"])
            
            self.userMedication.remove(at: indexPath.row)
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            self.tableView.endUpdates()
            self.tableView.reloadData()
            
            
        }
        deleteAction.backgroundColor = .red
        
        return [deleteAction]
    }
    //Need still to change the EDIT button for the correct size of the table view cell
    func tableView(_ tableView: UITableView,
                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let closeAction = UIContextualAction(style: .normal, title:  "Edit", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("OK, marked as Closed")
          //  let vc = EditVC()
          //  self.present(vc, animated: true, completion: nil)
          //  success(true)
        })
        closeAction.backgroundColor = .purple
        
        return UISwipeActionsConfiguration(actions: [closeAction])
        
    }
    //Seems to be working for resizing the delete slide button
     func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        self.tableView.subviews.forEach { subview in
            print("YourTableViewController: \(String(describing: type(of: subview)))")
            if (String(describing: type(of: subview)) == "UISwipeActionPullView") {
                if (String(describing: type(of: subview.subviews[0])) == "UISwipeActionStandardButton") {
                    var deleteBtnFrame = subview.subviews[0].frame
                    deleteBtnFrame.origin.y = 10
                    deleteBtnFrame.size.height = 55
                    
                    
                    // Subview in this case is the whole edit View
                    subview.frame.origin.y =  subview.frame.origin.y
                    subview.frame.size.height = 90
                    subview.subviews[0].frame = deleteBtnFrame
                    subview.backgroundColor = UIColor.red
                }
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        let medication = userMedication[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditVC") as! EditVC
        vc.infodetail = medication
        self.present(vc, animated: true, completion: nil)
    }
    
}

