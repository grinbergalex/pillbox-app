//
//  LoginMenu.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 30/11/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase

class LoginMenu: UIViewController {
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillHide() {
        self.view.frame.origin.y = 0
    }
    
    @objc func keyboardWillChange(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if emailTextfield.isFirstResponder || passwordTextfield.isFirstResponder {
                self.view.frame.origin.y = -keyboardSize.height + 100
            }
        }
    }
        
    
    @IBAction func login(_ sender: UIButton) {
        
        loginUser()
        
    }
    @IBAction func moveToRegister(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "RegisterMenu", sender: self)
    }
    func loginUser() {
        
        guard let email = emailTextfield.text, !email.isEmpty else { return }
        guard let password = passwordTextfield.text, !password.isEmpty else { return }
        
        if email.isEmpty == true || password.isEmpty == true {
            print("Fields are empty")
            self.Alert(title: "Fields are empty", message: "Make sure to fill the required fields")
        }
        
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error: Error?) in
            
            if let err = error {
                print("Failed to login:", err)
                return
            }
            
            print("Successfully created user:", user?.user.uid ?? "")
            
            self.performSegue(withIdentifier: "HomeVC", sender: self)
        })
    }
    

}
