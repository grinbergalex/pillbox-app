//
//  RegisterMenu.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 30/11/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class RegisterMenu: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        view .backgroundColor = UIColor.white
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillHide() {
        self.view.frame.origin.y = 0
    }
    
    @objc func keyboardWillChange(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if nameTextfield.isFirstResponder || emailTextfield.isFirstResponder || passwordTextfield.isFirstResponder {
                self.view.frame.origin.y = -keyboardSize.height + 100
            }
        }
    }

 
    
    @IBAction func Register(sender: AnyObject) {
        
        SVProgressHUD.show()
        saveUsers()
        SVProgressHUD.dismiss()
        self.performSegue(withIdentifier: "Terms", sender: self)
        
    }
    
    @IBAction func moveToLogin(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "LoginMenu", sender: self)
    }
    func saveUsers() {
        guard let name = nameTextfield.text else {return}
        guard let email = emailTextfield.text else {return}
        guard let password = passwordTextfield.text else {return}
        
        guard let image = self.profileImageView?.image else { return }
        
        let age = ""
        let weight = ""
        
        if nameTextfield.text!.isEmpty == true || emailTextfield.text!.isEmpty == true || passwordTextfield.text!.isEmpty == true {
            print("Fields are empty")
            self.registerBtn.isEnabled = false
            self.registerBtn.layer.opacity = 0.5
            self.Alert(title: "Fields are empty", message: "Please make sure to fill the mandatory fields")
            self.Alert(title: "Please fill required fields", message: "The fields are empty")
            
        } else {
            
            self.registerBtn.isEnabled = true
            
        }
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            
            if error != nil {
                print(error!)
                return
            }
            //successfully authenticated user

            
            guard let uploadData = image.jpegData(compressionQuality: 0.3) else { return }
            
            let filename = NSUUID().uuidString
            
            let storageRef = Storage.storage().reference().child("profile_images").child(filename)
            storageRef.putData(uploadData, metadata: nil, completion: { (metadata, err) in
                
                if let err = err {
                    print("Failed to upload profile image:", err)
                    return
                }
                
                // Firebase 5 Update: Must now retrieve downloadURL
                storageRef.downloadURL(completion: { (downloadURL, err) in
                    if let err = err {
                        print("Failed to fetch downloadURL:", err)
                        return
                    }
                    
                    guard let profileImageUrl = downloadURL?.absoluteString else { return }
                    guard let uid = Auth.auth().currentUser?.uid else { return }
                    
                    let dictionaryValues = ["name": name, "email": email,"password": password, "age": age, "weight": weight, "profileImageUrl": profileImageUrl, "provider": uid]
                    Database.database().reference().child("users").child(uid).updateChildValues(dictionaryValues, withCompletionBlock: { (err, ref) in
                        
                        if let err = err {
                            print("Failed to save user info into db:", err)
                            return
                        }
                    })
                    
                    print("Successfully uploaded profile image:\(profileImageUrl)")
                })
            })
        })
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func alertForUser() {
        let alert = UIAlertController(title: "User is registered successfully?", message: "Enjoy the app.", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    //Alert Function:
    func Alert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (action) in
            
            self.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
}

