//
//  WelcomeStartPage.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 02/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class WelcomeStartPage: UIViewController {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    
    @IBAction func loginSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "LoginMenu", sender: self)
    }
    
    @IBAction func registerSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "RegisterMenu", sender: self)
    }
    

}
