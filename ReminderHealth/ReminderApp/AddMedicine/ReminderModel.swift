//
//  ReminderModel.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 17/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit

class Reminder : NSObject {
    
    var reminder = ""
    
}

class MenuItems : NSObject {
    
    var name = ""
    var image: UIImage!
    
}

class History: NSObject {
    
    var medicineName = ""
    var medicineDosage = ""
    var medicineIndex = ""
}
