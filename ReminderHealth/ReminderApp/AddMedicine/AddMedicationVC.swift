//
//  AddMedicationVC.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 17/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import UserNotifications

class AddMedicationVC: UIViewController, UITextFieldDelegate, UIPickerViewDelegate {
    
    var reminders = [Reminder]()
    
    @IBOutlet weak var medicationNameTextField: UITextField!
    @IBOutlet weak var medicationDosageTextField: UITextField!
    @IBOutlet weak var medicationReminderTextField: UITextField!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var nameSeparatorView: UIView!
    @IBOutlet weak var dosageSeparatorView: UIView!
    @IBOutlet weak var reminderSeparatorView: UIView!
    @IBOutlet weak var saveMedicationToDB: UIButton!

    var notificationIndex: Int?
    
    var hours: String?
    var minutes: String?
    
    let uid = Auth.auth().currentUser?.uid
    let medicationId = Database.database().reference().childByAutoId().key
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.notificationIndex = 0
        
        view.backgroundColor = .white
        
  //      setupView()
        
        self.datePicker.isHidden = true
        
        hideKeyboardWhenTappedAround()
        
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]) { granted, error in
            if granted {
                print("authorized")
                
            }
        }
        
        
        
//        scheduleNotification()
        setupDatePicker()
    
    }
    @IBAction func startReminderSelection(_ sender: UITextField) {
        
        datePicker.isHidden = false
        saveMedicationToDB.isHidden = true
    }
    
    func setupDatePicker() {
        datePicker.datePickerMode = .time
        datePicker.contentMode = .center
        datePicker.backgroundColor = UIColor.rgb(red: 121, green: 182, blue: 225)
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
        datePicker.setValue(false, forKey: "highlightsToday")
//        datePicker.setValue(false, forKey: "separatorLine")
        
    }
    
    
    func saveMedications() {
       // SVProgressHUD.show()
        
        guard let name = medicationNameTextField.text, let dosage = medicationDosageTextField.text, let reminder = medicationReminderTextField.text else {return}

        if name.isEmpty || dosage.isEmpty  || reminder.isEmpty  {
          
                print("Fill required information")

        }
        
        let values = ["medicationName": name, "medicationDosage": dosage, "medicationReminder": reminder, "provider": uid, "medProvider": medicationId]
        
            let medicationReference = Database.database().reference().child("medications").child(uid!)
        medicationReference.child(medicationId!).updateChildValues(values as [AnyHashable : Any], withCompletionBlock: {(err, ref) in
                
            if err != nil {
                    print("Not Saved")
                } else {
                    print("Saved")
                    self.scheduleNotification()
                //Alerta that it was saved
                self.alertForUser()
                
                self.tabBarController?.selectedIndex = 0


                
                }
            
                
                
            })
 
    }
    
    @IBAction func startReminder(_ sender: UIDatePicker) {
        
        datePicker.isHidden = false
        saveMedicationToDB.isHidden = true
        
    }
    
     func touchesBegan(_ touches: Set<AnyHashable>, with event: UIEvent) {
        let touch: UITouch? = touches.first as? UITouch
        //location is relative to the current view
        // do something with the touched point
        if touch?.view != datePicker {
            datePicker.isHidden = true
        }
    }
    
    @IBAction func stopReminder(_ sender: UIDatePicker) {
        
        let date = DateFormatter()
        date.timeStyle = .short
        
        let dateString = date.string(from: datePicker.date)
        
        let dateAsString = (dateString)//"1:15 PM"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        let dates = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "HH:mm"
        
        let Date24 = dateFormatter.string(from: dates!)
        
        let hour = Date24.prefix(2)
        let minute = Date24.suffix(2)
        
        datePicker.isHidden = true
        saveMedicationToDB.isHidden = false
        
        hours = ("\(hour)")
        minutes = ("\(minute)")
        medicationReminderTextField.text = ("\(hour):\(minute)")
        datePicker.isHidden = true
        saveMedicationToDB.isHidden = false
    }
    //MARK: Need to check and make sure the local notifications are working.
    //MARK: Notification code seems to work. Integers for hour and minutes are produced.
    //MARK: Needs to test
    
    @IBAction func saveToDB(_ sender: UIButton) {
        
        saveMedications()
        
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
        
    }
    
    var dateFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        
        return formatter
    }()
    
    func scheduleNotification() {
        
        
        let center = UNUserNotificationCenter.current()
        
        let content = UNMutableNotificationContent()
        content.title = "Time to take your med"
        content.body = "Take your \(self.medicationReminderTextField.text ?? "medicine")"
        content.categoryIdentifier = "alarm"
        content.userInfo = ["customData": "fizzbuzz"]
        content.sound = UNNotificationSound.default
        
        var dateComponents = DateComponents()
        dateComponents.hour = Int(hours!)
        dateComponents.minute = Int(minutes!)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
    }
    
    @IBAction func skipButton(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "ContainerVC", sender: self)
        
    }
    
}

/*
 let tabCtrl: UITabBarController = segue.destination as! UITabBarController
 let destinationVC = tabCtrl.viewControllers![0] as! HomeViewController
 destinationVC.userObject = userObject[String!]  // In case you are using an array or something else in the object
 
 */
