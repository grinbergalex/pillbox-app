//
//  Terms.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 01/12/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class Terms: UIViewController {

    @IBOutlet weak var swtchOutlet: UISwitch!
    @IBOutlet weak var BackButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    

     //   swtchOutlet.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)

        // Do any additional setup after loading the view.
    }


    @IBAction func acceptedSwitched(_ sender: Any) {
        
        if swtchOutlet.isOn == true {
            self.performSegue(withIdentifier: "HomeVC", sender: self)
        } else {
            return
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.performSegue(withIdentifier: "RegisterMenu", sender: self)
    }
    

    
}
