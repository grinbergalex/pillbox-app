//
//  File.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 26/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit


class Slide: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UITextView!
    @IBOutlet weak var labelDesc: UITextView!
    @IBOutlet weak var startButton: UIButton!
    
}
