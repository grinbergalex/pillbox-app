//
//  EditVC.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 09/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase

class EditVC: UIViewController {
    
    @IBOutlet weak var medicationNameTextField: UITextField!
    @IBOutlet weak var medicationDosageTextField: UITextField!
    @IBOutlet weak var medicationReminderTextField: UITextField!
    
    @IBOutlet weak var saveEditSettings: UIButton!
    
    @IBOutlet weak var backButton: UIButton!

    
    
    var infodetail = UserMeds()
    var medicationIndex = ""
    let uid = Auth.auth().currentUser?.uid

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
 
        self.medicationNameTextField.text = infodetail.medName
        self.medicationDosageTextField.text = infodetail.medDosage
        self.medicationReminderTextField.text = infodetail.medReminder
        self.medicationIndex = infodetail.medIndex
        
      // print(Date().dayOfWeek()!) // Wednesday
        print(medicationIndex)
        
    }
    
    @IBAction func editMedicationAction(_ sender: Any) {
     
        self.performSegue(withIdentifier: "HomeVC", sender: self)
    }
    func updateUserInformation() {
        
        let values = ["medicationName": medicationNameTextField.text!, "medicationDosage": medicationDosageTextField.text!, "medicationReminder": medicationReminderTextField.text!]
        let ref = Database.database().reference().child("medications").child(uid!)
        ref.updateChildValues(values, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print("User edited successfully")
 
                self.tabBarController?.selectedIndex = 1
                
            } else {
                print("User cannot be edited")
            }
           
        })
        
    }

}
