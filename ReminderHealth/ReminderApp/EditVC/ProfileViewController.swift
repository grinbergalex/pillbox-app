//
//  ProfileViewController.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 19/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import FirebaseStorage

class ProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var saveProfileButton: UIButton!
    
    let uid = Auth.auth().currentUser?.uid
    
    var userInformation = [UserItemsInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImageView.isUserInteractionEnabled = true
        profileImageView.contentMode = .scaleAspectFit
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        
        fetchUserInfo()
        
    }

    func saveProfileEditions() {
        guard let name = nameTextField.text else {return}
        guard let email = emailTextField.text else {return}
        guard let password = emailTextField.text else {return}
        
        
        let values = ["name": name, "email": email, "password": password, "uid": uid!]
        
        let ref = Database.database().reference().child(uid!)
        ref.updateChildValues(values, withCompletionBlock: {(error, ref) in
            
            if error != nil {
                print("Worked")
                
            } else {
                self.Alert(title: "An error occurred", message: "Please try again to save it.")
                
            }
            
            
        })
        
        
    }
    
    func fetchUserInfo() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let childId = Database.database().reference().child("users").child(uid)
        childId.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let index = UserItemsInfo()
                index.pictureURL = ((dictionary["profileImageUrl"]) as? String)!
                index.name = ((dictionary["name"]) as? String)!
                index.email = ((dictionary["email"]) as? String)!
                index.password = ((dictionary["password"]) as? String)!
                
                self.userInformation.append(index)
                
                let url = URLRequest(url: URL(string: index.pictureURL)!)
                
                let task = URLSession.shared.dataTask(with: url) {
                    (data, response, error) in
                    
                    if error != nil {
                        print(error!)
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.profileImageView.image = UIImage(data: data!)
                        self.nameTextField.text = index.name
                        self.emailTextField.text = index.email
                        self.passwordTextField.text = index.password
                        
                    }
                }
                task.resume()
            }
            print(snapshot)
            
        }, withCancel: nil)
    }
    
    
    @objc func handleSelectProfileImageView() {
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImageFromPicker: UIImage?
        
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] {
            selectedImageFromPicker = editedImage as? UIImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage]{
            selectedImageFromPicker = originalImage as? UIImage
        }
        if let selectedImage = selectedImageFromPicker {
            self.profileImageView.image = selectedImage
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }


}
